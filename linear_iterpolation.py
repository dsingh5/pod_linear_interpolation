#!/usr/bin/env python3
import math
from PIL import Image
import glob
import numpy as np
from numpy import asarray
import cv2
import os   


a =input("Time interval between images")
t= input("Time at which image is to calculated")


cv_img = []
img_dir = os.getcwd() # Enter Directory of all images 
data_path = os.path.join(img_dir,'*.png')
files = glob.glob(data_path)

for img in files:
    temp= cv2.imread(img)
    #temp = np.array(list(temp.getdata(band=0)), float)
    temp = np.array(temp)
    print(temp.shape)
    cv_img.append(temp)

f,index = math.modf(t/a)    
index = int(index)     
print(index)
print((cv_img))                       #integer part and fractional part 
req_img = cv_img[index]
for i in range(len(cv_img[index])):
    for j in range(len(cv_img[index][0])):
        req_img[i][j]+=np.add(req_img[i][j],((cv_img[index+1][i][j]-cv_img[index][i][j])*f),out =req_img[i][j],casting = "unsafe")  # calculating final image

cv2.imwrite("Output_img.png",req_img)
