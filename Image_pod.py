#!/usr/bin/env python3

""" Reduces the Image size by applying SVD techniques
"""

import matplotlib.pyplot as plt
import numpy as np
import time
import cv2
from PIL import Image
import math
import glob
import os

img_dir = os.getcwd() # Enter Directory of all images 
data_path = os.path.join(img_dir,'*.TIF')
files = glob.glob(data_path)
# read all images in directory
n=500

for img in files: 
    temp= Image.open(img)
    
    grey=temp.convert('LA')                                 #convert into grey
    temp = np.array(list(grey.getdata(band=0)), float)
    temp.shape = (grey.size[1], grey.size[0])               #appplying svd
    temp = np.matrix(temp)
    print(temp)
    U, sigma, V = np.linalg.svd(temp)
    U = U[0:n,:]
    V = V[:,0:n]
    temp = np.matrix(U) * np.diag(sigma[:]) * np.matrix(V)
    print(temp.shape)
    img = img[:-3] +"png"                                   #convert to _png
    cv2.imwrite(img,temp)
    print(img)
    #temp.save(img)"""

# implmeting a linear interpolation model
