.. Mowito Rosbot Documentation documentation master file, created by
   sphinx-quickstart on Wed Jun 17 01:43:46 2020.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to Mowito Rosbot documentation!
========================================================



Set up GitLab account
^^^^^^^^^^^^^^^^^^^^^
   Add ssh keys to your GitLab account
   For help look at this `link <https://docs.gitlab.com/ee/ssh/>`_ .
   
   
   [optional] install catkin build tools from `here <https://catkin-tools.readthedocs.io/en/latest/installing.html>`_
.. toctree::
   :maxdepth: 2
   :caption: Contents:

      



Setting Navigation Core
^^^^^^^^^^^^^^^^^^^^^^^

   1. In home create a directory mowito_ws/src.::
         
         mkdir -p ~/mowito_ws/src

   2. Inside src, clone the following repo.::

         git clone git@gitlab.com:mowito_dev/mw_tools.git

   3. Pull all the repo.::
         
         ~/mowito_ws/src/mw_tools/scripts/update.sh master

   4. Go to mowito_ws, and do.::
   
         rosdep install --from-paths src --ignore-src -r -y

   5. Build using.::

         cd mowito_ws && catkin build

   6. Change the primitive_filename in config file in mw_sbpl to point to the file in your PC

   

.. Indices and tables
.. ==================

.. * :ref:`genindex`
.. * :ref:`modindex`
.. * :ref:`search`
